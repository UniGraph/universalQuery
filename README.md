# Universal Query

Universal Query or UQL is a Global decentralized high performance Graph & Database!
It's main models are Graph, Relational, and Columnar - using TiDB, Janus, Aerospike. And it uses technologies to increase performance like Infinispan, Redis, VoltDB